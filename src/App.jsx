import React, { useState } from "react";
import { Container, Row, Col } from 'react-bootstrap';
import About from "./components/About/About";
import Education from "./components/Education/Education";
import Experience from "./components/Experience/Experience";
import Me from "./components/Me/Me";
import More from "./components/More/More";
import ScrollArrow from "./components/ScrollArrow/ScrollArrow";
import { CV } from "./CV/CV";
import "./App.css";


const {
  me,
  education,
  experience,
  languages,
  softSkills,
  hardSkills,
  otherActivities,
} = CV;


export default function App() {
  const [showEducation, setShowEducation] = useState(true);

  return (
    <Container className="App">
      <Row>
        <Col lg={8} md={8} sm={6} xs={6} className="responsiveXs">
          <Me me={me} />
          <About aboutMe={me} />

          <div className="educationExpBtnDiv">
            <div class="separatorEduExp">
              <div class="line"></div>
                <button
                  className="active custom-btn btn-4 educationExpBtnDiv educationBtn"
                  onClick={() => setShowEducation(true)}
                >
                  Education
                </button>
                <button
                  className="custom-btn btn-4 educationExpBtnDiv expBtn"
                  onClick={() => setShowEducation(false)}
                >
                  Experience
                </button>
              <div class="line"></div>
            </div>
          </div>
          
          <div>
            {showEducation ? (
              <div className="alignDivs">
                <Education education={education} />
              </div>
            ) : (
              <div className="alignDivs">
                <Experience experience={experience} />
              </div>
            )}
          </div>
          <More
            languages={languages}
            softSkills={softSkills}
            hardSkills={hardSkills}
            otherActivities={otherActivities}
          />

          <div className="scrollArrowDiv">
            <ScrollArrow />
          </div>

          </Col>
        </Row>
    </Container>
  );
}
