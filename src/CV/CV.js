export const CV = {
    me: {
      name: "Ana Margarida",
      adress: "Costa",
      city: "Caldas da Rainha, Portugal",
      email: "ana.masaco@gmail.com",
      birthDate: "24/01/1994",
      phone: "(+351) 960000363",
      image: "https://i.ibb.co/Bn2TbsN/Ana.jpg",
      gitLab: "https://gitlab.com/ana.masaco",
      aboutMe: [
        {
          info: "I'm Ana Margarida Costa, I have 27 years old and I'm a Frontend Developer!",
        },
        {
          info: "I like music, photography, painting, reading, writing and animals.",
        },
      ],
    },
    education: [
      {
        name: "Frontend Bootcamp from Upgrade Hub and Indra",
        date: "09/2021 - 10/2021",
        where: "Remote",
      },
      {
        name: "IEFP Course - Digital Marketing",
        date: "06/2021 - 07-2021",
        where: "Remote",
      },
      {
        name: "CENCAL - EFA (level 4)",
        date: "02/2020 - 03/2021",
        where: "CENCAL and Remote",
      },
      {
        name: "IEFP Course - Communication and Entrepreneurship",
        date: "07/2019 - 09/2019",
        where: "AIRO, Caldas da Rainha",
      },
      {
        name: "IEFP Course - Marketing e Business German (not completed)",
        date: "05/2018 - 06/2018",
        where: "AIRO, Caldas da Rainha",
      },
      {
        name: "Graduation in HROC",
        date: "2013 - 06/2016",
        where: "Polytechnic Institute of Leiria",
      },
    ],
    experience: [
      {
        name: "Frontend Curricular Internship",
        date: "02/2021 - 03/2021",
        where: "Tempos Brilhantes",
        description:
          "In my internship I worked with HTML5, Bootstrap, CSS3, Javascript and jQuery. First experience with Frontend.",
      },
      {
        name: "Profissional Internship",
        date: "06/2018 - 03/2019",
        where: "Predial Maritoni",
        description:
          "In this job I did mostly Administration, but I always complemented it with: Marketing, Online Content Management and Accounting.",
      },
      {
        name: "Administrative worker",
        date: "05/2017 - 11/2017",
        where: "Smart Homes",
        description:
          "In this job I did mostly Administration, but I always complemented it with: Marketing, Online Content Management, Accounting and  Housing Tourism.",
      },
      {
        name: "Curricular Internship",
        date: "03/2016 - 06/2016",
        where: "interMEDIAR",
        description:
          "In this job I did mostly Administration, but I always complemented it with: Communication & Media, Events Organisation, MARL, Telework.",
      },
      {
        name: "Customer service",
        date: "2013",
        where: "My Story",
        description:
          "In this job I did Customer service on a clothes shop (Part-time).",
      },
    ],
    languages: [
      {
        language: "Portuguese",
        wrlevel: "Native",
        splevel: "Native",
      },
      {
        language: "English",
        wrlevel: "B2",
        splevel: "B2",
      },
      {
        language: "French",
        wrlevel: "B1",
        splevel: "B1",
      },
    ],
    softSkills: [
      "Creativity",
      "Team work",
      "Communication",
      "Organization",
      "Commitment",
      "Ethic",
      "Dating",
      "Integrity",
      "Empathy",
      "Self-taught",
    ],
    hardSkills: [
      "HTML",
      "Bootstrap",
      "CSS",
      "SASS",
      "JavaScript",
      "React js",
      "PHP",
      "SQL",
    ],
    otherActivities: [
      {
        name: "Play Cello",
        description:
          "It was a childhood dream that I was finally able to make true.",
      },
      {
        name: "Photography",
        description:
          "I love to take photos of what I think it's a beautiful moment to remember later. In 2021 I've created an Instagram page where I share it.",
      },
    ],
  };
  