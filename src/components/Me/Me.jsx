import React from "react";
import "./Me.css";

import { FaGitlab, FaAddressCard, FaCalendarAlt, FaEnvelope, FaGlobeEurope, FaPhoneAlt } from 'react-icons/fa';

const Me = ({ me }) => {
  return (
    <div className="meContainer">
      
      <div className="meCard">
      <img src={me.image} alt="myself"/>
        <div className="meCardTopics">
          <FaAddressCard className="faIcon"/> 
          <h1>
            {me.name} {me.adress}
          </h1>
        </div>
        <div className="meCardTopics">
          <FaGlobeEurope className="faIcon"/> 
          <p>
            {me.city}
          </p>
        </div>
        <div className="meCardTopics">
          <FaCalendarAlt className="faIcon"/> 
          <p>
            {me.birthDate}
          </p>
        </div>
        <div className="meCardTopics">
          <FaEnvelope className="faIcon"/> 
          <p>
            <a href={"email:" + me.email}> ana.masaco@gmail.com</a>
          </p>
        </div>
        <div className="meCardTopics">
          <FaPhoneAlt className="faIcon"/> 
          <p>
            {me.phone}
          </p>
        </div>
        <div className="meCardTopics">
          <p>Check my GitLab Account
            <a href={me.gitLab}>
              <FaGitlab className="faIcon faIconGit"/>
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Me;
