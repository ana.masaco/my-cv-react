import React from "react";
import { FaBuilding } from 'react-icons/fa';
import "./Experience.css";

const Experience = ({ experience }) => {
  return (
    <div>
      <div className="experienceCard">
        {experience.map((item) => {
          return (
            <div className="expTextDiv" key={JSON.stringify(item)}>
              <h4><FaBuilding/> {item.name}</h4>
              <p>{item.date}</p>
              <p> <strong>{item.where}</strong></p>
              <p>{item.description}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Experience;
