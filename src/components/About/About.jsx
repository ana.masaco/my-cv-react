import React from "react";
import { FaMusic, FaCamera, FaPaintBrush, FaBookReader, FaPenFancy, FaCat } from 'react-icons/fa';
import "./AboutMe.css";

const About = ({ aboutMe }) => {
  return (
    <div className="aboutMeContainer">
      <div class="separator">
        <div class="line"></div>
        <h1 style={{ color: "white" }}>About Me</h1>
        <div class="line"></div>
      </div>

      <div className="aboutMeCard">
        {aboutMe.aboutMe.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p>{item.info}</p>
            </div>
          );
        })}
        <p>
        <FaMusic className="faIconAboutMe"/> 
        <FaCamera className="faIconAboutMe"/> 
        <FaPaintBrush className="faIconAboutMe"/> 
        <FaBookReader className="faIconAboutMe"/> 
        <FaPenFancy className="faIconAboutMe"/> 
        <FaCat className="faIconAboutMe"/></p>
      </div>
    </div>
  );
};

export default About;
