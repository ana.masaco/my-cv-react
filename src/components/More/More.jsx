import React from "react";
import { FaLanguage } from 'react-icons/fa';
import "./Languages.css";
import "./OtherActivities.css";
import "./Skills.css";

export default function More({ languages, softSkills, hardSkills, otherActivities }) {
  return (
    <div className="moreDiv">

      <div class="separatorMore">
        <div class="lineMore"></div>
        <h1 className="languageTextDiv" style={{ color: "white" }}>
          <div className="moreLanguageIcon">
            <FaLanguage/>
          </div>
          Languages
        </h1>
        <div class="lineMore"></div>
      </div>

      <div className="languageCard">
        {languages.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h2>{item.language}</h2>
              <p>
                <strong>Writing: </strong>
                {item.wrlevel}
              </p>
              <p>
                <strong>Speacking: </strong> {item.splevel}
              </p>
            </div>
          );
        })}
      </div>

      <div class="separatorMore">
        <div class="lineMore"></div>
        <h1 style={{ color: "white" }}>Soft Skills</h1>
        <div class="lineMore"></div>
      </div>

      <div className="skillsCard">
        {softSkills.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h4>{item}</h4>
            </div>
          );
        })}
      </div>
      
      <div class="separatorMore">
        <div class="lineMore"></div>
        <h1 style={{ color: "white" }}>Hard Skills</h1>
        <div class="lineMore"></div>
      </div>

      <div className="skillsCard">
        {hardSkills.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h4>{item}</h4>
            </div>
          );
        })}
      </div>
      
      <div class="separatorMore">
        <div class="lineMore"></div>
        <h1 style={{ color: "white" }}>Other Activities</h1>
        <div class="lineMore"></div>
      </div>

      <div className="otherActivitiesCard">
        {otherActivities.map((item) => {
          return (
            <div className="otherActivitiesTextDiv" key={JSON.stringify(item)}>
            <div className="otherActivitiesText" >
              <h4>{item.name}</h4>
              <p>{item.description}</p>
            </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
